#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "libdebug.h"
#include "libudp.h"

//==============================================================================

int setup_udp_socket(int* fd, uint16_t port_no)
{
    // Create UDP socket:
    if((*fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) return EXIT_FAILURE;

    if(setsockopt(*fd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int))) {
        P_ERR("failed setting SO_REUSEADDR, errno: %d (%s)", errno, strerror(errno));
        close(*fd);
        return EXIT_FAILURE;
    }

    if(setsockopt(*fd, SOL_SOCKET, SO_REUSEPORT, &(int){1}, sizeof(int))) {
        P_ERR("failed setting SO_REUSEPORT, errno: %d (%s)", errno, strerror(errno));
        close(*fd);
        return EXIT_FAILURE;
    }

    // Bind socket:
    struct sockaddr_in my_addr = {0};

    my_addr.sin_family = AF_INET;
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    my_addr.sin_port = htons(port_no);

    if(bind(*fd, (struct sockaddr*)&my_addr, sizeof(my_addr))) {
        P_ERR("bind failed, errno: %d (%s)", errno, strerror(errno));
        close(*fd);
        return EXIT_FAILURE;
    }

    if(fcntl(*fd, F_SETFL, O_CLOEXEC)) {
        P_ERR("failed to set CLOEXEC, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//=========================================================================================================================

int setup_udp_socket_NB(int* fd, uint16_t port_no)
{
    if(setup_udp_socket(fd, port_no)) {
        P_ERR_STR("failed to setup udp socket");
        return EXIT_FAILURE;
    }

    if(fcntl(*fd, F_SETFL, O_NONBLOCK | O_CLOEXEC)) {
        P_ERR("failed to set NONBLOCK and CLOEXEC, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//=========================================================================================================================

// Wrapper for messageUdpSocket with tx_len set to 1
int ping_udp_socket_fd(int fd, uint16_t port_no, uint8_t ping)
{
    return message_udp_socket_fd(fd, port_no, &ping, 1);
}

//=========================================================================================================================

int message_udp_socket_fd(int fd, uint16_t port_no, const void* message, size_t tx_len)
{
    // Use specified stream (i/p arg1) to send message to specified port_no (i/p arg2):
    struct sockaddr_in pingaddr = {0};

    pingaddr.sin_family = AF_INET;
    pingaddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    pingaddr.sin_port = htons(port_no);

    ssize_t sresplen = sendto(fd, message, tx_len, 0, (struct sockaddr*)&pingaddr, sizeof(pingaddr));

    // Check if sending message was successful:
    if(sresplen == -1) {
        P_ERR("sendto failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    } else if(sresplen < (ssize_t)tx_len) {
        P_ERR("sendto failed, expected %zu got %zu", tx_len, (size_t)sresplen);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//==============================================================================

int message_udp_socket(uint16_t port_no, const void* message, size_t tx_len)
{
    int fd;
    if(setup_udp_socket(&fd, 0)) {
        P_ERR_STR("failed setting up udp socket");
        return EXIT_FAILURE;
    }
    
    if(message_udp_socket_fd(fd, port_no, message, tx_len)) {
        P_ERR_STR("failed sending message");
        close(fd);
        return EXIT_FAILURE;
    }

    close(fd);

    return EXIT_SUCCESS;
}

//==============================================================================

int message_udp_socket_NB(uint16_t port_no, const void* message, size_t tx_len)
{
    int fd;
    if(setup_udp_socket_NB(&fd, 0)) {
        P_ERR_STR("failed setting up non-blocking udp socket");
        return EXIT_FAILURE;
    }
    
    if(message_udp_socket_fd(fd, port_no, message, tx_len)) {
        P_ERR_STR("failed sending message");
        close(fd);
        return EXIT_FAILURE;
    }

    close(fd);

    return EXIT_SUCCESS;
}

//=========================================================================================================================

int reply_on_socket_fd(int fd, const void* tx, size_t tx_len, struct sockaddr_in* from_addr, socklen_t from_len)
{
    // Send reply back on socket:
    ssize_t actual_tx_len = sendto(fd, tx, tx_len, 0, (struct sockaddr *)from_addr, from_len);

    if(actual_tx_len == -1) {
        P_ERR("sendto failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    } else if(actual_tx_len < (ssize_t)tx_len) {
        P_ERR("sendto failed, expected %zu got %zu", tx_len, (size_t)actual_tx_len);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//==============================================================================

int read_udp_packet_fd(int fd, void* resp, size_t* resp_len, struct sockaddr_in* from_addr, socklen_t* from_len, struct timeval* timeout)
{
    fd_set rset;

    FD_ZERO(&rset);
    FD_SET(fd, &rset);

    int nready;
    if((nready = select(fd + 1, &rset, NULL, NULL, timeout)) < 0) {
        P_ERR("select failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if(!FD_ISSET(fd, &rset)) {
        P_ERR_STR("FD_ISSET failed");
        return EXIT_FAILURE;
    }

    ssize_t rx_len = recvfrom(fd, resp, *resp_len, 0, (struct sockaddr*)from_addr, from_len);
    
    if(rx_len < 0) {
        P_ERR("recvfrom failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    *resp_len = (size_t)rx_len;
    
    // Fail if we got nothing, or resp_len > 0 and rx_len != resp_len
    if(rx_len == 0 && *resp_len != 0) {
        P_ERR_STR("no bytes received, client shutdown?");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//==============================================================================

int read_udp_packet_NB_fd(int fd, void* resp, size_t* resp_len, struct sockaddr_in* from_addr, socklen_t* from_len)
{
    if(read_udp_packet_fd(fd, resp, resp_len, from_addr, from_len, &(struct timeval){0})) {
        P_ERR_STR("failed to read udp packet");
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

//==============================================================================

int send_msg_and_recv_response_fd(int fd, uint16_t dest_port, const void* msg, size_t msg_len, void* resp, size_t* resp_len, struct timeval* timeout)
{
    if(message_udp_socket_fd(fd, dest_port, msg, msg_len)) {
        P_ERR("failed to message port %"PRIu16, dest_port);
        return EXIT_FAILURE;
    }

    usleep(10000); // Wait this much at a minimum for them to respond
    
    if(read_udp_packet_fd(fd, resp, resp_len, NULL, NULL, timeout)) {
        P_ERR_STR("read udp packet failed");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//=========================================================================================================================

int send_msg_and_recv_response(uint16_t dest_port, const void* msg, size_t msg_len, void* resp, size_t* resp_len, struct timeval* timeout)
{
    int fd;
    if(setup_udp_socket(&fd, 0)) {
        P_ERR_STR("failed to setup udp socket");
        return EXIT_FAILURE;
    }

    if(send_msg_and_recv_response_fd(fd, dest_port, msg, msg_len, resp, resp_len, timeout)) {
        close(fd);
        return EXIT_FAILURE;
    }

    close(fd);

    return EXIT_SUCCESS;
}

//=========================================================================================================================

int send_msg_and_recv_response_NB(uint16_t dest_port, const void* msg, size_t msg_len, void* resp, size_t* resp_len, struct timeval* timeout)
{
    int fd;
    if(setup_udp_socket_NB(&fd, 0)) {
        P_ERR_STR("failed to setup udp socket");
        return EXIT_FAILURE;
    }

    if(send_msg_and_recv_response_fd(fd, dest_port, msg, msg_len, resp, resp_len, timeout)) {
        close(fd);
        return EXIT_FAILURE;
    }

    close(fd);

    return EXIT_SUCCESS;
}

//=========================================================================================================================

