#ifndef LIB_UDP_H
#define LIB_UDP_H

#include <stdint.h>
#include <stdlib.h>

int setup_udp_socket(int* fd, uint16_t port_no);
int setup_udp_socket_NB(int* fd, uint16_t port_no);

int ping_udp_socket_fd(int fd, uint16_t port_no, uint8_t ping);

int message_udp_socket_fd(int fd, uint16_t port_no, const void* message, size_t tx_len);

/**
 * Good for infrequent messages, otherwise open up a udp socket and used message_udp_socket_fd
 */
int message_udp_socket(uint16_t port_no, const void* message, size_t tx_len);
int message_udp_socket_NB(uint16_t port_no, const void* message, size_t tx_len);

int reply_on_socket_fd(int fd, const void* tx, size_t tx_len, struct sockaddr_in* from_addr, socklen_t from_len);

int read_udp_packet_fd(int fd, void* resp, size_t* resp_len, struct sockaddr_in* from_addr, socklen_t* from_len, struct timeval* timeout);
int read_udp_packet_NB_fd(int fd, void* resp, size_t* resp_len, struct sockaddr_in* from_addr, socklen_t* from_len);

/**
 * Core function for UDP clients to use to message daemons.
 * Send msg to dest_port via fd, using timeout to wait.
 * Read response into resp if available, and check if response
 * has a length matching resp_len.
 *
 * It is legal for msg and resp to overlap.
 *
 * If resp_len is 0, any response is considered acceptable.
 */
int send_msg_and_recv_response_fd(int fd, uint16_t dest_port, const void* msg, size_t msg_len, void* resp, size_t* resp_len, struct timeval* timeout);

/**
 * Good for infrequent messages, otherwise open up a udp socket and used send_msg_and_recv_response_fd
 */
int send_msg_and_recv_response(uint16_t dest_port, const void* msg, size_t msg_len, void* resp, size_t* resp_len, struct timeval* timeout);
int send_msg_and_recv_response_NB(uint16_t dest_port, const void* msg, size_t msg_len, void* resp, size_t* resp_len, struct timeval* timeout);

#endif
